//
//  Departments.swift
//  rahaty
//
//  Created by Amr Elsayed on 2/26/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import  BFPaperButton
import  Alamofire
import ObjectMapper
import XLActionController
import  ImageSlideshow
import SWFrameButton

class Home: UIViewController  ,CLLocationManagerDelegate,MKMapViewDelegate ,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager:CLLocationManager = CLLocationManager()
    var currentLocation:CLLocation?
    var pickedLocation:CLLocationCoordinate2D?

    @IBOutlet weak var search_btn: SWFrameButton!
    var gestureRecognizer:UITapGestureRecognizer=UITapGestureRecognizer()

    @IBOutlet weak var nav_bar: UINavigationBar!
    @IBOutlet weak var btn_reveal: UIBarButtonItem!
    
    var cities_array:[City]=NSArray() as! [City]
    var city_id = "0";
    @IBOutlet weak var city_name_txt: UILabel!
    
    var country_array:[City]=NSArray() as! [City]
    var country_id = "0";
    @IBOutlet weak var country_name_txt: UILabel!
    
    var depts_array:[Department]=NSArray() as! [Department]
    var dept_id = "0";
    var dept_name = "0";

    @IBOutlet weak var dept_name_txt: UILabel!
    
    var adsDetailsImages: [AdsImage]?

    @IBOutlet weak var images_slider: ImageSlideshow!

    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuHelper.setupMenu(view: self, btn_reveal: btn_reveal)
        updateUi()

      //  mapView.delegate = self
        self.locationManager.distanceFilter  = 1000
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation=true
     //   mapView.showsCompass=true
      //  mapView.showsScale=true
       // mapView.showsTraffic=true
        
        gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(Home.handleTap))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
        
        self.country_name_txt.text=Strings.Data.Country[Defaults.getLanguageIndex()]
        self.city_name_txt.text=Strings.Data.City[Defaults.getLanguageIndex()]
        self.dept_name_txt.text=Strings.Data.Department[Defaults.getLanguageIndex()]
        
        
        callServerToGetAdsImages()
        
    }
    
    override  func viewWillAppear(_ animated: Bool) {
        clearMap()
        pickedLocation=nil
    }
    
    func updateUi() {
        
        nav_bar.topItem?.title=Strings.Data.Location[Defaults.getLanguageIndex()]
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())
        search_btn.setTitle(Strings.Data.search[Defaults.getLanguageIndex()], for: .normal)
    }
    
    @objc func handleTap() {
        clearMap()
        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        pickedLocation=coordinate
    }
    
    func clearMap()  {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorizationStatus \(status)")
        
        if status == .authorizedWhenInUse  {
            print(".Authorized")
            self.locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        currentLocation = locations.last
        if currentLocation != nil{
            print("there is location ")
            centerMapOnLocation(currentLocation!)
        }
        
    }
    
    func centerMapOnLocation(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  Double(5) * 2.0, Double(5) * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    

    func callServerToGetAdsImages(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        let  url=Url.adsImagesURL
        print(url)
        connection.startConnectionWithMapping(view: self,model:AdsImage.self,url:url , parameter:nil, callback: {
            result in
            
            self.adsDetailsImages=result
            self.updateImagesSlider()
            self.callServerToGetCountery()

            
        })
    }
    
    func updateImagesSlider()  {
        
        let sdWebImageSource:NSMutableArray=NSMutableArray()
        
        if((self.adsDetailsImages?.count)!>0)
        {
            for item in (self.adsDetailsImages)!
            {
                let urlwithPercentEscapes = item.image?.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                sdWebImageSource.add(SDWebImageSource(urlString: urlwithPercentEscapes!)!)
            }
            
        }
        else
        {
            sdWebImageSource.add(ImageSource(imageString: "logo")!)
        }
        images_slider.backgroundColor = UIColor.white
        images_slider.slideshowInterval = 5.0
        images_slider.pageControlPosition = PageControlPosition.underScrollView
        images_slider.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        images_slider.pageControl.pageIndicatorTintColor = UIColor.black
        images_slider.contentScaleMode = UIViewContentMode.scaleAspectFill
        images_slider.setImageInputs(sdWebImageSource as! [InputSource] )
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        images_slider.addGestureRecognizer(recognizer)
        
    }
    
    @objc func didTap() {
        let fullScreenController = images_slider.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    
    func callServerToGetCities(country_id:String)
        
    {
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        var url:String=""
        if(country_id == "")
        {
            url=Url.citiesAllUrl
        }
        else
        {
            url=Url.citiesUrl+"/"+country_id
        }
        connection.startConnectionWithMapping(view: self,model:City.self,url: url, parameter:nil, callback: {
            result in
            
            self.cities_array.removeAll()
                self.cities_array=result
            self.cities_array.append(City(id: "", name: Strings.Data.All[Defaults.getLanguageIndex()]))
//                if(self.cities_array.count>0)
//                {
//                    self.city_name_txt.text=self.cities_array[0].name
//                    self.city_id = self.cities_array[0].id!
//                }
                self.callServerToGetDepts()
            
        })
    }
    
    func callServerToGetCountery()
        
    {
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        connection.startConnectionWithMapping(view: self,model:City.self,url: Url.countryUrl, parameter:nil, callback: {
            result in
            
            self.country_array.removeAll()
            self.country_array=result
            self.country_array.append(City(id: "", name: Strings.Data.All[Defaults.getLanguageIndex()]))
//                if(self.country_array.count>0)
//                {
//                    self.country_name_txt.text=self.country_array[0].name
//                    self.country_id = self.country_array[0].id!
//                }
            self.callServerToGetCities(country_id: self.country_id)
            
        })
    }
    
    func callServerToGetDepts()
        
    {
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        connection.startConnectionWithMapping(view: self,model:Department.self,url: Url.sectionsUrl, parameter:nil, callback: {
            result in
            
            self.depts_array.removeAll()
                self.depts_array=result
            self.depts_array.append(Department(id: "", name: Strings.Data.All[Defaults.getLanguageIndex()]))
//                if(self.depts_array.count>0)
//                {
//                    self.dept_name_txt.text=self.depts_array[0].name
//                    self.dept_id = self.depts_array[0].id!
//                }
            
        })
    }
    
    @IBAction func cityButtonClick(_ sender: Any) {
        
        let actionController = SkypeActionController()
        actionController.backgroundView.backgroundColor=UIColor.purple
        actionController.collectionView.isScrollEnabled=true
        
        if(self.cities_array.count>0)
        {
            for i in 0...cities_array.count-1 {
                
                actionController.addAction(Action(self.cities_array[i].name, style: ActionStyle.default, handler: { action in
                    
                    self.city_name_txt.text=self.cities_array[i].name
                    self.city_id = self.cities_array[i].id!
                }))
            }
            present(actionController, animated: true, completion: nil)
        }
    }
    
    @IBAction func ccountryButtonClick(_ sender: Any) {
        
        let actionController = SkypeActionController()
        actionController.backgroundView.backgroundColor=UIColor.purple
        actionController.collectionView.isScrollEnabled=true
        
        if(self.country_array.count>0)
        {
            for i in 0...country_array.count-1 {
                
                actionController.addAction(Action(self.country_array[i].name, style: ActionStyle.default, handler: { action in
                    
                    self.country_name_txt.text=self.country_array[i].name
                    self.country_id = self.country_array[i].id!
                    self.callServerToGetCities(country_id: self.country_id)

                }))
            }
            present(actionController, animated: true, completion: nil)
        }
    }
    
    @IBAction func deptButtonClick(_ sender: Any) {
        
        let actionController = SkypeActionController()
        actionController.backgroundView.backgroundColor=UIColor.purple
        actionController.collectionView.isScrollEnabled=true
        
        if(self.depts_array.count>0)
        {
            for i in 0...depts_array.count-1 {
                
                actionController.addAction(Action(self.depts_array[i].name, style: ActionStyle.default, handler: { action in
                    
                    self.dept_name_txt.text=self.depts_array[i].name
                    self.dept_id = self.depts_array[i].id!
                    self.dept_name=self.depts_array[i].name!
                }))
            }
            present(actionController, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    @IBAction func sendOrderBtnClicked(_ sender: Any) {
        
        if(pickedLocation == nil && country_id == "0")
        {
            SweetAlert().showAlert("", subTitle: "يجب اختيار عناصر البحث او تحديد الموقع على الخريطة اولا", style: AlertStyle.warning)

        }
        else
        {
            self.performSegue(withIdentifier: "map", sender: self)

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "map" {
            let dc = segue.destination as! ProvidersMap
            dc.city_id=self.city_id
            dc.country_id=self.country_id
            dc.dept_id=self.dept_id
            dc.pickedLocation=self.pickedLocation
            if(self.dept_id == "")
            {
                dc.dept_name=Strings.Data.All_depts[Defaults.getLanguageIndex()]

            }
            else if(self.dept_id == "0")
            {
                dc.dept_name=Strings.Data.Service_Provider[Defaults.getLanguageIndex()]
                
            }
            else
            {
                dc.dept_name=self.dept_name
            }
            
        }
        
    }


}
