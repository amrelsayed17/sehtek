//
//  Profile.swift
//  Nobil
//
//  Created by Amr Elsayed on 8/20/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import  BFPaperButton
import  Alamofire
import XLActionController

class Profile: UIViewController , UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var btn_reveal: UIBarButtonItem!
    
    @IBOutlet weak var nav_bar: UINavigationBar!

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var password_confirm: UITextField!
    
    @IBOutlet weak var confirm_btn: BFPaperButton!

    
    var icon_image:UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SideMenuHelper.setupMenu(view: self, btn_reveal: btn_reveal)

        updateUi()
        
        self.name.text=(UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.nameuser
        self.phone.text=(UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.mobile
        
    }
    
    func updateUi() {

        confirm_btn.setTitle(Strings.Data.Confirm[Defaults.getLanguageIndex()], for: .normal)
        nav_bar.topItem?.title=Strings.Data.profile[Defaults.getLanguageIndex()]
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func signUpButtonClick(_ sender: Any) {
        
        if(name.text=="")
        {
            SweetAlert().showAlert("", subTitle:Strings.Data.Invalid_Name[Defaults.getLanguageIndex()], style: AlertStyle.warning)
            
        }
        else if(phone.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.invalid_phone[Defaults.getLanguageIndex()], style: AlertStyle.warning)
            
        }
        else if(password.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.invalid_password[Defaults.getLanguageIndex()], style: AlertStyle.warning)
            
        }
        else if(password.text != password_confirm.text)
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.Password_Not_Confirmed[Defaults.getLanguageIndex()], style: AlertStyle.error)
        }
        else
        {
            callServerToUpdateProfile()
        }
        
    }
 
    func callServerToUpdateProfile(){
        let user:UserModel=(UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as! UserModel)
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params = ["name":name.text!  , "mobile": phone.text! , "password": password.text!, "email": "a"  , "city": "9" , "country": "7","id":user.iduser ?? ""]

        print("bbbbbbbbbbbbbbb",params)
        
        connection.startConnectionWithGeneralMapping(view: self,model:GeneralResponseModel.self,url: Url.profileUrl , parameter:params, callback: {
            result in
            
            let response:GeneralResponseModel = result as! GeneralResponseModel
            if(Defaults.checkResponse(response: response) )
            {
                let model:UserModel=UserModel()
                model.mobile=self.phone.text!
                model.password=self.password.text!
                model.nameuser=self.name.text!
                model.email=user.email ?? ""
                model.type="1"
                model.iduser=user.iduser!
                
                UserDefultsHelper.saveObjectDefault(key: UserDefultsHelper.USER_MODEL, value: model)
                SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.success)
                
            }
            else
            {
                SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.error)
            }
            
        })
    }


}
