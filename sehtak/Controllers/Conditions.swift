//
//  Conditions.swift
//  Nobil
//
//  Created by Amr Elsayed on 9/25/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import Alamofire

class Conditions: UIViewController {
    
    @IBOutlet weak var btn_reveal: UIBarButtonItem!
    @IBOutlet weak var nav_bar: UINavigationBar!
    @IBOutlet weak var info: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SideMenuHelper.setupMenu(view: self, btn_reveal: btn_reveal)
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())
        nav_bar.topItem?.title=Strings.Data.Conditions[Defaults.getLanguageIndex()]

        getInfoFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getInfoFromServer()  {
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        var url:String=""
        if(Defaults.getLanguage()=="ar")
        {
            url=Url.gettermsconditionsappUrl
        }
        else
        {
            url=Url.gettermsconditionsappUrl+"en"
            
        }
        connection.startConnectionWithGeneralMapping(view: self,model:AboutUsModel.self,url:url, parameter:nil, callback: {
            result in
            
            let response:AboutUsModel = result as! AboutUsModel
            self.info.attributedText=NSAttributedString(html: (response.text)!)
            
        })
    }
    
}
