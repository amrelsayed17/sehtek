//
//  ProductDetails.swift
//  azooz
//
//  Created by Amr Elsayed on 3/26/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import SWFrameButton
import MIBadgeButton_Swift
import DTPhotoViewerController
import Alamofire
import  ImageSlideshow

class ProductDetails: UIViewController {

    @IBOutlet weak var p_details: UITextView!
    @IBOutlet weak var p_price: SWFrameButton!
    @IBOutlet weak var images_slider: ImageSlideshow!

    var provider:Provider = Provider()
    var sponser:SponserModel = SponserModel()

    @IBOutlet weak var details_lbl: UILabel!
    @IBOutlet weak var nav_abr: UINavigationBar!
    @IBOutlet weak var show_btn: SWFrameButton!
    @IBOutlet weak var print_btn: SWFrameButton!
    @IBOutlet weak var share_btn: SWFrameButton!
    @IBOutlet weak var general_btn: SWFrameButton!
    var adsDetailsImages: [AdsImage]?

    var code:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.p_price.setTitle((Strings.Data.discount[Defaults.getLanguageIndex()] + provider.discount!) + " % " , for: .normal)
        print((provider.discount!) + " % خصم ")
        self.p_details.attributedText=NSAttributedString(html: provider.details ?? "")

        if(Defaults.getLanguage() == "ar")
        {
            self.p_details.textAlignment = .right
        }
        else
        {
            self.p_details.textAlignment = .left

        }

        callServerToGetAdsImages()
        
        show_btn.setTitle(Strings.Data.show[Defaults.getLanguageIndex()], for: .normal)
        print_btn.setTitle(Strings.Data.print[Defaults.getLanguageIndex()], for: .normal)
        share_btn.setTitle(Strings.Data.friend[Defaults.getLanguageIndex()], for: .normal)
        nav_abr.topItem?.title=provider.name ?? ""
        details_lbl.text=Strings.Data.Details[Defaults.getLanguageIndex()]
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())

        getInfoFromServer()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func callServerToGetAdsImages(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let  url=Url.getserviceproviderimages
        let params=["id":provider.id ?? ""]
        print(url)
        print(params)
        connection.startConnectionWithGeneralMapping(view: self,model:AdsImageModel.self,url:url , parameter:params, callback: {
            result in
            
            var response:AdsImageModel=result as! AdsImageModel
            self.adsDetailsImages=response.data ?? Array()
            self.updateImagesSlider()
            
            
        })
    }
    
    func updateImagesSlider()  {
        
        let sdWebImageSource:NSMutableArray=NSMutableArray()
        
        if((self.adsDetailsImages?.count)!>0)
        {
            for item in (self.adsDetailsImages)!
            {
                let urlwithPercentEscapes = item.p_image?.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                sdWebImageSource.add(SDWebImageSource(urlString: urlwithPercentEscapes!)!)
            }
            
        }
        else
        {
            sdWebImageSource.add(ImageSource(imageString: "logo")!)
        }
        images_slider.backgroundColor = UIColor.white
        images_slider.slideshowInterval = 5.0
        images_slider.pageControlPosition = PageControlPosition.underScrollView
        images_slider.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        images_slider.pageControl.pageIndicatorTintColor = UIColor.black
        images_slider.contentScaleMode = UIViewContentMode.scaleAspectFill
        images_slider.setImageInputs(sdWebImageSource as! [InputSource] )
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        images_slider.addGestureRecognizer(recognizer)
        
    }
    
    @objc func didTap() {
        let fullScreenController = images_slider.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    @IBAction func showBtnClicked(_ sender: Any) {
        if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser != nil
        {
            if(code == "")
            {
                callServerToGetCode(index: 1)
            }
            else
            {
                SweetAlert().showAlert("Code : ", subTitle: code, style: AlertStyle.success)

            }
        }
        else
        {
        //  Dialog.needToLoginDialog(sender: self)
            Defaults.def=false
            self.performSegue(withIdentifier: "login", sender: self)
        }
    }
    
    @IBAction func printBtnClicked(_ sender: Any) {
        if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser != nil
        {
            if(code == "")
            {
                callServerToGetCode(index: 2)
            }
            else
            {
                SweetAlert().showAlert("Code : ", subTitle: self.code, style: AlertStyle.success, buttonTitle:Strings.Data.ok[Defaults.getLanguageIndex()], buttonColor:UIColor.darkGray , otherButtonTitle: nil, otherButtonColor: nil) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        
                        self.takeScreenshot(true)
                        
                    }
                    
                }
            }
        }
        else
        {
           // Dialog.needToLoginDialog(sender: self)
            Defaults.def=false
            self.performSegue(withIdentifier: "login", sender: self)
        }
    }
    
    @IBAction func shareBtnClicked(_ sender: Any) {
        if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser != nil
        {
            if(code == "")
            {
                callServerToGetCode(index: 3)
            }
            else
            {
                            let message = "تطبيق لصحتك"
                            if let link = NSURL(string: Url.BaseURL)
                            {
                                let objectsToShare = [message,code] as [Any]
                                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                                self.present(activityVC, animated: true, completion: nil)
                            }
            }

        }
        else
        {
           // Dialog.needToLoginDialog(sender: self)
            Defaults.def=false
            self.performSegue(withIdentifier: "login", sender: self)
        }
    }
    
    func callServerToGetCode(index:Int){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params = ["sp_id":provider.id!,"ben_id":(UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as! UserModel).iduser! ]
        print(params)
        connection.startConnectionWithGeneralMapping(view: self,model:GeneralResponseModel.self,url: Url.requestcouponUrl, parameter:params, callback: {
            result in
            
            if(result != nil)
            {
                let response:GeneralResponseModel = result as! GeneralResponseModel
                if(Defaults.checkResponse(response: response) )
                {
                    self.code=response.code!
                    
                    if(index == 1)
                    {
                        SweetAlert().showAlert("Code : ", subTitle: self.code, style: AlertStyle.success)

                    }
                    else if(index == 2)
                    {
                        SweetAlert().showAlert("Code : ", subTitle: self.code, style: AlertStyle.success, buttonTitle:Strings.Data.ok[Defaults.getLanguageIndex()], buttonColor:UIColor.darkGray , otherButtonTitle: nil, otherButtonColor: nil) { (isOtherButton) -> Void in
                            if isOtherButton == true {
                                
                                self.takeScreenshot(true)

                            }
                            
                        }
                    }
                    else
                    {
                        let message = "تطبيق لصحتك"
                        if let link = NSURL(string: Url.BaseURL)
                        {
                            let objectsToShare = [message,self.code] as [Any]
                            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                            self.present(activityVC, animated: true, completion: nil)
                        }
                    }

                }
                else
                {
                    SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.error)
                }
                
            }
        })
        
    }
    
    func getInfoFromServer()  {
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        connection.startConnectionWithMapping(view: self,model:SponserModel.self,url: Url.getallsponsoredlinkUrl , parameter:nil, callback: {
            result in
            
            if(result.count>0)
            {
                self.sponser=result[0]
                self.general_btn.setTitle(self.sponser.name, for: .normal)
            }
            else
            {
                self.general_btn.isHidden=true
            }
            
        })
    }
    
    open func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        return screenshotImage
    }
    
    
    @IBAction func generalBtnClicked(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: (self.sponser.link)!)!) {
            UIApplication.shared.open(URL(string: (self.sponser.link)!)!, options: [:], completionHandler: nil)
        }
    }
    

    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
}
