//
//  SignUp.swift
//  Nobil
//
//  Created by Amr Elsayed 
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import Alamofire
import XLActionController
import  SWFrameButton

class SignUp: UIViewController {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var confirm_btn: SWFrameButton!

    

    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
    }
    
    func updateUi() {
        phone.placeholder=Strings.Data.Phone[Defaults.getLanguageIndex()]
        password.placeholder=Strings.Data.Password[Defaults.getLanguageIndex()]
        name.placeholder=Strings.Data.Name[Defaults.getLanguageIndex()]
        email.placeholder=Strings.Data.Email[Defaults.getLanguageIndex()]
        confirm_btn.setTitle(Strings.Data.Signup[Defaults.getLanguageIndex()], for: .normal)
        self.navigationItem.title=Strings.Data.Sign_Up[Defaults.getLanguageIndex()]

        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpButtonClick(_ sender: Any) {
        if(name.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.Invalid_Name[Defaults.getLanguageIndex()], style: AlertStyle.warning)
        }
        else if(email.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.Invalid_Email[Defaults.getLanguageIndex()], style: AlertStyle.warning)
        }
        else if(phone.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.invalid_phone[Defaults.getLanguageIndex()], style: AlertStyle.warning)
        }
        else if(password.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.invalid_password[Defaults.getLanguageIndex()], style: AlertStyle.warning)
        }
        else
        {
            callServerToSignUp()
        }
        
    }
    
    func callServerToSignUp(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params = ["name":name.text! , "email": email.text! , "mobile": phone.text! , "password": password.text! , "city": "2" , "country": "1"]
        
        connection.startConnectionWithGeneralMapping(view: self,model:GeneralResponseModel.self,url: Url.signUpUrl, parameter:params, callback: {
            result in
            
            let response:GeneralResponseModel = result as! GeneralResponseModel
            if(Defaults.checkResponse(response: response) )
            {
                SweetAlert().showAlert( response.message! , subTitle: "يمكنك تسجيل الدخول الان" , style: AlertStyle.success)
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.error)
            }
            
        })
    }


}
