//
//  ProvidersMap.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/1/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Alamofire
import BFPaperButton
import SWFrameButton
import XLActionController

class ProvidersMap: UIViewController ,MKMapViewDelegate, UISearchBarDelegate {
    
    var pickedLocation:CLLocationCoordinate2D?
    var city_id = "";
    var country_id = "";
    var dept_id = "";
    var dept_name = ""
    var providers_array:[Provider]=NSArray() as! [Provider]
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var search_bar: UISearchBar!

    @IBOutlet weak var map_btn: SWFrameButton!
    @IBOutlet weak var company_btn: SWFrameButton!
    @IBOutlet weak var nav_bar: UINavigationBar!
    
    var dataSourceForSearchResult:[Provider]=NSArray() as! [Provider]
    
    var searchBarActive:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate=self
        callServerToGetProviders()
        
        map_btn.setTitle(Strings.Data.map[Defaults.getLanguageIndex()], for: .normal)
        company_btn.setTitle(Strings.Data.companies[Defaults.getLanguageIndex()], for: .normal)
     //   nav_bar.topItem?.title=Strings.Data.Service_Provider[Defaults.getLanguageIndex()]
         nav_bar.topItem?.title=dept_name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callServerToGetProviders(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        
        var params:[String:String]?
        var url:String=""
        if(pickedLocation == nil)
        {
            params=["country_id":country_id , "city_id" : city_id, "cat_id" : dept_id]
            url=Url.providersUrl
        }
        else
        {
            params=["latitude":"\(pickedLocation?.latitude ?? 0.0 )" , "longitude" :"\( pickedLocation?.longitude  ?? 0.0)"]
            url=Url.providersLocUrl
        }
        print(params)
        
        connection.startConnectionWithMapping(view: self,model:Provider.self,url: url, parameter:params, callback: {
            result in
            
            self.providers_array=result
            if(self.providers_array.count>0)
            {
                self.updateMap(data: self.providers_array)
            }
            else
            {
                SweetAlert().showAlert("", subTitle: "لا توجد بيانات", style: AlertStyle.warning)

            }
            
        })

    }
    
    func updateMap(data:[Provider])
    {
        self.mapView.removeAnnotations(self.mapView.annotations)
        for  i in data{
            if( (i.latitude?.count)!>0 && (i.longitude?.count)!>0)
            {
                print(i.latitude)
                let worker = Worker(title: i.name ?? "",
                                    _id:  i.id ?? "",
                                    phone: i.mobile ?? "",
                                    coordinate: CLLocationCoordinate2D(latitude: (i.latitude! as NSString).doubleValue , longitude:  (i.longitude! as NSString).doubleValue ),email:i.email ?? "")
                
                self.mapView.addAnnotation(worker)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Worker {
            let identifier = "pin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                // 3
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.image = UIImage(named: "pin")
                view.canShowCallout = true
                view.isEnabled=true
              //  view.calloutOffset = CGPoint(x: -5, y: 5)
                 view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            return view
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
       // self.performSegue(withIdentifier: "details", sender: view.annotation)
//        let actionController = SkypeActionController()
//        actionController.addAction(Action(Strings.Data.Show_details[Defaults.getLanguageIndex()], style: ActionStyle.default, executeImmediatelyOnTouch: false , handler: { action in
//
//            DispatchQueue.main.async {
//                self.performSegue(withIdentifier: "details", sender: view.annotation)
//            }
//        }))
//
//        actionController.addAction(Action(Strings.Data.Show_location[Defaults.getLanguageIndex()], style: ActionStyle.default, handler: { action in
//
            let provider:Worker=(view.annotation as! Worker )
//
//            let regionDistance:CLLocationDistance = 1000000
//            let coordinates = CLLocationCoordinate2DMake(provider.coordinate.latitude, provider.coordinate.longitude)
//            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
//            let options = [
//                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
//                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span),
//                MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving
//                ] as [String : Any]
//
//            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
//            let mapItem = MKMapItem(placemark: placemark)
//            mapItem.name = provider.title
//            mapItem.openInMaps(launchOptions: options)
            
//            let url = "http://maps.apple.com/maps?saddr=My Location"
//            UIApplication.shared.openURL(URL(string:url)!)
        
        let alertController = UIAlertController(title: provider.title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        let DestructiveAction = UIAlertAction(title: Strings.Data.cancel[Defaults.getLanguageIndex()], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
        }
        
        let okAction = UIAlertAction(title: Strings.Data.Show_details[Defaults.getLanguageIndex()], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "details", sender: view.annotation)
            
        }
        
        let locAction = UIAlertAction(title: Strings.Data.Show_location[Defaults.getLanguageIndex()], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
           let url = "https://www.google.com/maps?saddr=\(mapView.userLocation.coordinate.latitude),\(mapView.userLocation.coordinate.longitude)&daddr=\(provider.coordinate.latitude),\(provider.coordinate.longitude)"
            
        //    let url = "https://www.google.com/maps?saddr=My+Location&daddr=\(provider.coordinate.latitude),\(provider.coordinate.longitude)"
             self.open(scheme: url)
            print(url)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(locAction)
        alertController.addAction(DestructiveAction)

        present(alertController, animated: true, completion: nil)
    
        

//
//
//
//        }))
//
//        present(actionController, animated: true, completion: nil)
        
    }
    
    func open(scheme: String) {
        print(scheme)

        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
    
    func filterContentForSearchText(searchText:String){
        self.dataSourceForSearchResult = self.providers_array.filter({ (provider:Provider) -> Bool in
            return (provider.name?.contains(searchText))!
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // user did type something, check our datasource for text that looks the same
        if searchText.characters.count > 0 {
            // search and reload data source
            self.searchBarActive    = true
            self.filterContentForSearchText(searchText: searchText)
            self.updateMap(data: dataSourceForSearchResult)

            
        }else{
            // if text lenght == 0
            // we will consider the searchbar is not active
            self.searchBarActive = false
            self.updateMap(data: providers_array)

            
        }
        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self .cancelSearching()
        self.updateMap(data: providers_array)

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarActive = true
        self.view.endEditing(true)
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        // this method is being called when search btn in the keyboard tapped
        // we set searchBarActive = NO
        // but no need to reloadCollectionView
        //   self.searchBarActive = false
        self.search_bar!.setShowsCancelButton(false, animated: false)
    }
    func cancelSearching(){
        self.searchBarActive = false
        self.search_bar!.resignFirstResponder()
        self.search_bar!.text = ""
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "list" {
            let dc = segue.destination as! ProvidersList
            dc.city_id=self.city_id
            dc.country_id=self.country_id
            dc.dept_id=self.dept_id
            dc.pickedLocation=self.pickedLocation
            dc.providers_array=self.providers_array
            dc.dept_name=self.dept_name

        }
        else if segue.identifier == "details" {
            let dc = segue.destination as! ProductDetails
            let worker:Worker=sender as! Worker
            
            if(searchBarActive)
            {
                for i in dataSourceForSearchResult
                {
                    if(i.id == worker._id)
                    {
                        dc.provider=i
                    }
                }            }
            else
            {
                for i in providers_array
                {
                    if(i.id == worker._id)
                    {
                        dc.provider=i
                    }
                }
            }
            
           
        }
    }

}
