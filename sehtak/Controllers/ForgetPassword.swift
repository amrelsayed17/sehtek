//
//  ForgetPassword.swift
//  Nobil
//
//  Created by Amr Elsayed 
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import Alamofire
import BFPaperButton

class ForgetPassword: UIViewController {

    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var send_bt: BFPaperButton!
    @IBOutlet weak var email_message_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
    }
    
    func updateUi() {
        email_message_lbl.text=Strings.Data.phone_message[Defaults.getLanguageIndex()]
        send_bt.setTitle(Strings.Data.Send[Defaults.getLanguageIndex()], for: .normal)
        self.navigationItem.title=Strings.Data.Forget_Password[Defaults.getLanguageIndex()]

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func sendButtonClick(_ sender: Any) {
        
        if(email.text=="")
        {
            SweetAlert().showAlert("", subTitle:Strings.Data.Invalid_Email[Defaults.getLanguageIndex()], style: AlertStyle.warning)
            
        }
        else
        {
            callServerToGetPassword()
        }
    }
    
    func callServerToGetPassword(){
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params = ["mobile":email.text!]
        connection.startConnectionWithGeneralMapping(view: self,model:GeneralResponseModel.self,url: Url.forgetPasswordUrl, parameter:params, callback: {
            result in
            
            let response:GeneralResponseModel = result as! GeneralResponseModel
            if(Defaults.checkResponse(response: response) )
            {
                SweetAlert().showAlert( "" , subTitle: response.message! , style: AlertStyle.success)
            }
            else
            {
                SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.error)
            }
            
        })
    }
    
    
}
