//
//  SelectLanguage.swift
//  Nobil
//
//  Created by Amr Elsayed 
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit

class SelectLanguage: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
                
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func arabicBtnClicked(_ sender: Any) {
        UserDefaults.standard.set("ar", forKey: "lang")
        LogHelper.startMainHome()
    }

    @IBAction func englishBtnClicked(_ sender: Any) {
        UserDefaults.standard.set("en", forKey: "lang")
        LogHelper.startMainHome()
    }
}
