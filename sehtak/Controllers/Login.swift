//
//  Login.swift
//  Nobil
//
//  Created by Amr Elsayed 
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import Alamofire
import  SWFrameButton


class Login: UIViewController {

    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var forget_password_btn: UIButton!
    
    @IBOutlet weak var login_btn: SWFrameButton!
    @IBOutlet weak var singup_btn: SWFrameButton!
    
    @IBOutlet weak var skip_btn: SWFrameButton!
    
    @IBOutlet weak var type_segment: UISegmentedControl!
    
    var type:String="1" // 1-> cient  , 2-> provider
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
    }
    
    func updateUi() {
       
        type_segment.setTitle(Strings.Data.client[Defaults.getLanguageIndex()], forSegmentAt: 0)
       type_segment.setTitle(Strings.Data.Service_Provider[Defaults.getLanguageIndex()], forSegmentAt: 1)
        phone.placeholder=Strings.Data.Phone[Defaults.getLanguageIndex()]
        password.placeholder=Strings.Data.Password[Defaults.getLanguageIndex()]
        forget_password_btn.setTitle(Strings.Data.Forgrt[Defaults.getLanguageIndex()], for: .normal)
        login_btn.setTitle(Strings.Data.Login[Defaults.getLanguageIndex()], for: .normal)
        skip_btn.setTitle(Strings.Data.Skip[Defaults.getLanguageIndex()], for: .normal)

        singup_btn.setTitle(Strings.Data.Signup[Defaults.getLanguageIndex()], for: .normal)
        self.navigationItem.title=Strings.Data.Login[Defaults.getLanguageIndex()]
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func skipBtnClicked(_ sender: Any) {
        if(Defaults.def==true)
        {
            Defaults.def=true
            LogHelper.startMainHome()
            
        }
        else
        {
            Defaults.def=true
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func loginButtonClick(_ sender: Any) {
        if(phone.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.invalid_phone[Defaults.getLanguageIndex()], style: AlertStyle.warning)
            
        }
        else if(password.text=="")
        {
            SweetAlert().showAlert("", subTitle: Strings.Data.invalid_password[Defaults.getLanguageIndex()], style: AlertStyle.warning)
            
        }
        else
        {
            callServerToLogin()
        }
    }
    
    func callServerToLogin(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params = ["mobile":phone.text!,"password": password.text!]
        var url:String=""
        if(type=="1")
        {
            url=Url.loginUrl
        }
        else
        {
            url=Url.login_providerUrl
            
        }
        print(url , params)
        connection.startConnectionWithGeneralMapping(view: self,model:UserModel.self,url:url , parameter:params, callback: {
            result in
            
            if(result != nil)
            {
                let response:UserModel = result as! UserModel
                if(Defaults.checkResponse(response: GeneralResponseModel.init(message: response.message!, status: response.status!)) )
                {
                    SweetAlert().showAlert( "" , subTitle: response.message! , style: AlertStyle.success)
                    response.mobile=self.phone.text!
                    response.password=self.password.text!
                    response.email="a"
                    response.type=self.type
                    UserDefultsHelper.saveObjectDefault(key: UserDefultsHelper.USER_MODEL, value: response)
                    
                    if(Defaults.def==true)
                    {
                        Defaults.def=true
                        LogHelper.startMainHome()

                    }
                    else
                    {
                        Defaults.def=true
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else
                {
                    SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.error)
                }
                
            }
        })
        
    }

    @IBAction func typeChanged(_ sender: Any) {
        
        var seg:UISegmentedControl=sender as! UISegmentedControl
        if(seg.selectedSegmentIndex == 0)
        {
            type="1"
        }
        else
        {
            type="2"
        }
    }
    
}
