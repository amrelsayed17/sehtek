//
//  Coupons.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/2/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import Alamofire

class Coupons: UIViewController , UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var btn_reveal: UIBarButtonItem!
    @IBOutlet weak var nav_bar: UINavigationBar!
    @IBOutlet weak var table_view: UITableView!
    
    var coup_array:[CouponModel]=NSArray() as! [CouponModel]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuHelper.setupMenu(view: self, btn_reveal: btn_reveal)
        nav_bar.topItem?.title=Strings.Data.MyCart[Defaults.getLanguageIndex()]
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())

        callServerToGetMyOrders()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coup_array.count
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AlertCell
        cell.initCell(coupone: coup_array[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    

    
    func callServerToGetMyOrders(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params=["ben_id":(UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as! UserModel).iduser!  ]
        print(params)
        
        connection.startConnectionWithMapping(view: self,model:CouponModel.self,url: Url.getallcouponuserappUrl, parameter:params, callback: {
            result in
            
            self.coup_array=result
            if(self.coup_array.count>0)
            {
                self.table_view.reloadData()
            }
            else
            {
                SweetAlert().showAlert("", subTitle: "لا توجد بيانات", style: AlertStyle.warning)
                
            }
            
        })
    }
    

    
}
