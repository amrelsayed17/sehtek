//
//  ProvidersList.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/1/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import Alamofire
import BFPaperButton
import MIBadgeButton_Swift
import SWFrameButton
import CoreLocation
import MapKit

class ProvidersList: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UISearchBarDelegate {
    
    @IBOutlet weak var collection_view: UICollectionView!
        @IBOutlet weak var search_bar: UISearchBar!
    var pickedLocation:CLLocationCoordinate2D?

    var city_id = "";
    var country_id = "";
    var dept_id = "";
    var providers_array:[Provider]=NSArray() as! [Provider]
    var dept_name = ""

    @IBOutlet weak var map_btn: SWFrameButton!
    @IBOutlet weak var company_btn: SWFrameButton!
    @IBOutlet weak var nav_bar: UINavigationBar!
    
    var dataSourceForSearchResult:[Provider]=NSArray() as! [Provider]

    var searchBarActive:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
     //   callServerToGetProviders()
        
        map_btn.setTitle(Strings.Data.map[Defaults.getLanguageIndex()], for: .normal)
        company_btn.setTitle(Strings.Data.companies[Defaults.getLanguageIndex()], for: .normal)
    //    nav_bar.topItem?.title=Strings.Data.Service_Provider[Defaults.getLanguageIndex()]
        nav_bar.topItem?.title=dept_name

        collection_view.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.searchBarActive {
            return self.dataSourceForSearchResult.count;
        }
        return providers_array.count

    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 5 * 3) / 3 //some width
        return CGSize(width: width, height: 151);
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OfferCell
        
        if(searchBarActive)
        {
            cell.initCell(offer: dataSourceForSearchResult[indexPath.row])
        }
        else
        {
            cell.initCell(offer: providers_array[indexPath.row])
        }
        
       // cell.initCell(offer: providers_array[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "details", sender: collectionView.cellForItem(at: indexPath))
        
    }
    
    func callServerToGetProviders(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        var params:[String:String]?
        var url:String=""
        if(pickedLocation == nil)
        {
            params=["country_id":country_id , "city_id" : city_id, "cat_id" : dept_id]
            url=Url.providersUrl
        }
        else
        {
            params=["latitude":"\(pickedLocation?.latitude ?? 0.0 )" , "longitude" :"\( pickedLocation?.longitude  ?? 0.0)"]
            url=Url.providersLocUrl
        }
        print(params)
        
        connection.startConnectionWithMapping(view: self,model:Provider.self,url: url, parameter:params, callback: {
            result in
            
            self.providers_array=result
            if(self.providers_array.count>0)
            {
                  self.collection_view.reloadData()
            }
            else
            {
                SweetAlert().showAlert("", subTitle: "لا توجد بيانات", style: AlertStyle.warning)
                
            }
            
        })
        
    }
    
    func filterContentForSearchText(searchText:String){
        self.dataSourceForSearchResult = self.providers_array.filter({ (provider:Provider) -> Bool in
            return (provider.name?.contains(searchText))!
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // user did type something, check our datasource for text that looks the same
        if searchText.characters.count > 0 {
            // search and reload data source
            self.searchBarActive    = true
            self.filterContentForSearchText(searchText: searchText)
            self.collection_view?.reloadData()

        }else{
            // if text lenght == 0
            // we will consider the searchbar is not active
            self.searchBarActive = false
            self.collection_view?.reloadData()
            
        }
        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self .cancelSearching()
        self.collection_view?.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarActive = true
        self.view.endEditing(true)
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        // this method is being called when search btn in the keyboard tapped
        // we set searchBarActive = NO
        // but no need to reloadCollectionView
     //   self.searchBarActive = false
        self.search_bar!.setShowsCancelButton(false, animated: false)
    }
    func cancelSearching(){
        self.searchBarActive = false
        self.search_bar!.resignFirstResponder()
        self.search_bar!.text = ""
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details" {
            let dc = segue.destination as! ProductDetails
            let cell:OfferCell = sender as! OfferCell
          //  dc.provider=providers_array[(collection_view.indexPath(for: cell)?.row)!]
            if(searchBarActive)
            {
                dc.provider=dataSourceForSearchResult[(collection_view.indexPath(for: cell)?.row)!]
            }
            else
            {
                dc.provider=providers_array[(collection_view.indexPath(for: cell)?.row)!]

            }
        
        }
        
    }
}


