//
//  SearchCoupone.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/3/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import Alamofire
import  SWFrameButton

class SearchCoupone: UIViewController {

    @IBOutlet weak var provider: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var card_code: UITextField!
    
    @IBOutlet weak var enter_code_lbl: UILabel!
    @IBOutlet weak var search_btn: SWFrameButton!
    @IBOutlet weak var provider_lbl: UILabel!
    @IBOutlet weak var owner_lbl: UILabel!
    @IBOutlet weak var status_lbl: UILabel!
    @IBOutlet weak var btn_reveal: UIBarButtonItem!

    @IBOutlet weak var nav_bar: UINavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuHelper.setupMenu(view: self, btn_reveal: btn_reveal)
        search_btn.setTitle(Strings.Data.search_coupone[Defaults.getLanguageIndex()], for: .normal)
        nav_bar.topItem?.title=Strings.Data.search_coupone[Defaults.getLanguageIndex()]
        provider_lbl.text=Strings.Data.card_provider[Defaults.getLanguageIndex()]
        owner_lbl.text=Strings.Data.Card_Owner[Defaults.getLanguageIndex()]
        status_lbl.text=Strings.Data.Status[Defaults.getLanguageIndex()]
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())

        
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func callServerToGetCardInfo(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        let params=["code": card_code.text! ]

        connection.startConnectionWithMapping(view: self,model:CardModel.self,url: Url.searchcouponUrl, parameter:params, callback: {
            result in
            
            if(result.count>0)
            {
               // self.name.text=result[0].user_name
                self.provider.text=result[0].service_provider
                if(result[0].active == "0")
                {
                    self.state.text="غير محجوز"

                }
                else
                {
                    self.state.text="محجوز"

                }
                
                if(result[0].user_name == nil)
                {
                    self.name.text="غير مستخدم"
                    
                }
                else
                {
                    self.name.text=result[0].user_name
                    
                }
                
            }
            else
            {
                SweetAlert().showAlert("", subTitle: "لا توجد بيانات", style: AlertStyle.warning)
                
            }
            
        })
    }
    
    
    @IBAction func performBtnClicked(_ sender: Any) {
        
        if(card_code.text=="")
        {
            SweetAlert().showAlert("", subTitle: "من فضلك ادخل البيانات المطلوبه", style: AlertStyle.warning)
            
        }
        else
        {
            callServerToGetCardInfo()
            
        }
    }
}
