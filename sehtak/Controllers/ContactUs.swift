//
//  ContactUs.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/15/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import Alamofire
import  BFPaperButton
import Foundation
import MessageUI
import SWFrameButton

class ContactUs: UIViewController , MFMailComposeViewControllerDelegate{
    @IBOutlet weak var btn_reveal: UIBarButtonItem!
    @IBOutlet weak var nav_bar: UINavigationBar!
    
    @IBOutlet weak var titlel: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var details: UITextField!
    
    @IBOutlet weak var phone_lbl: UILabel!
    @IBOutlet weak var email: UILabel!
    
    var social:SettingsModel?
    
    @IBOutlet weak var send_btn: SWFrameButton!
    @IBOutlet weak var type_segment: UISegmentedControl!
    
    var type:String = ""
    
    @IBAction func typeChanged(_ sender: Any) {
        var seg:UISegmentedControl=sender as! UISegmentedControl
        if(seg.selectedSegmentIndex == 0)
        {
            type="1"
        }
        else if(seg.selectedSegmentIndex == 1)
        {
            type="2"
        }
        else if(seg.selectedSegmentIndex == 2)
        {
            type="3"
        }
    }
    
    @IBAction func sendBtnClicked(_ sender: Any) {
        if(titlel.text=="" || details.text=="" || phone.text=="")
        {
            SweetAlert().showAlert("", subTitle: "من فضلك ادخل البيانات المطلوبه", style: AlertStyle.warning)
            
        }
        else
        {
            callServerToSendMessage()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
        SideMenuHelper.setupMenu(view: self, btn_reveal: btn_reveal)
        getInfoFromServer()
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(phoneClicked(sender:)))
        phone_lbl.isUserInteractionEnabled = true
        phone_lbl.addGestureRecognizer(tap1)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(emailClicked(sender:)))
        email.isUserInteractionEnabled = true
        email.addGestureRecognizer(tap3)
        
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        

    }
    
    @objc
    func phoneClicked(sender:UITapGestureRecognizer) {
        
        if let url = URL(string: "tel://\(self.social?.mob! ?? "00")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @objc
    func emailClicked(sender:UITapGestureRecognizer) {
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([(self.social?.siteemail!)!])
        composeVC.setSubject("Message Subject")
        composeVC.setMessageBody("Message content.", isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }
    
    
    func updateUi() {
        nav_bar.topItem?.title=Strings.Data.Contact[Defaults.getLanguageIndex()]
        type_segment.setTitle(Strings.Data.Enquiry[Defaults.getLanguageIndex()], forSegmentAt: 0)
        type_segment.setTitle(Strings.Data.Suggestion[Defaults.getLanguageIndex()], forSegmentAt: 1)
        type_segment.setTitle(Strings.Data.Complaint[Defaults.getLanguageIndex()], forSegmentAt: 2)

        phone.placeholder=Strings.Data.Phone[Defaults.getLanguageIndex()]
        titlel.placeholder=Strings.Data.Name[Defaults.getLanguageIndex()]
        details.placeholder=Strings.Data.Details[Defaults.getLanguageIndex()]
        send_btn.setTitle(Strings.Data.Confirm[Defaults.getLanguageIndex()], for: .normal)
        ViewsHelper.viewDirections(self.view, lang: Defaults.getLanguage())

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callServerToSendMessage(){
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.post
        var url:String=""
        if(type=="1")
        {
            url=Url.enquiryUrl
        }
        else if(type == "2")
        {
            url=Url.suggestionUrl
            
        }
        else
        {
            url=Url.complaintUrl

        }
        let params = ["name":titlel.text!,"details": details.text!,"mobile":phone.text!]
        connection.startConnectionWithGeneralMapping(view: self,model:GeneralResponseModel.self,url: url, parameter:params, callback: {
            result in
            
            let response:GeneralResponseModel = result as! GeneralResponseModel
            if(Defaults.checkResponse(response: response) )
            {
                SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.success)
                
                
            }
            else
            {
                SweetAlert().showAlert("", subTitle: response.message, style: AlertStyle.error)
            }
            
        })
    }
    
    func getInfoFromServer()  {
        
        let connection:Connection=Connection()
        connection.method=HTTPMethod.get
        connection.startConnectionWithMapping(view: self,model:SettingsModel.self,url: Url.getcontactinfoUrl , parameter:nil, callback: {
            result in

                
                self.social=result[0]
            self.phone_lbl.text=Strings.Data.Phone[Defaults.getLanguageIndex()]+" : "+((self.social!.mob)!)
            self.email.text=Strings.Data.Email[Defaults.getLanguageIndex()]+" : "+(self.social!.siteemail!)
            
        })
    }
    
    
    @IBAction func twitBtnClicked(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: (self.social?.twitter!)!)!) {
            UIApplication.shared.open(URL(string: (self.social?.twitter!)!)!, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func faceBtnClicked(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: (self.social?.facebook!)!)!) {
            UIApplication.shared.open(URL(string: (self.social?.facebook!)!)!, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func instaBtnClicked(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: (self.social?.instagram!)!)!) {
            UIApplication.shared.open(URL(string: (self.social?.instagram!)!)!, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func youBtnClicked(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: (self.social?.youtube!)!)!) {
            UIApplication.shared.open(URL(string: (self.social?.youtube!)!)!, options: [:], completionHandler: nil)
        }
    }
    
 
    
    
    
}
