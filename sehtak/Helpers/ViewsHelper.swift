

import UIKit

class ViewsHelper: NSObject {
    static  func viewDirections(_ v:UIView,lang:String){
        v.semanticContentAttribute = (lang == "en") ? .forceLeftToRight : .forceRightToLeft
        if v is UILabel{
            ( v as! UILabel ).textAlignment = (lang == "en") ? .left : .right
        }
        if v is UITextField{
            ( v as! UITextField ).textAlignment = (lang == "en") ? .left : .right

        }

        for c in v.subviews{
            viewDirections(c,lang:lang)
        }
    }
}
