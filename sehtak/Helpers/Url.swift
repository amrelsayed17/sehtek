//
//  Url.swift
//  Nobil
//
//  Created by Art4muslim on 7/7/17.
//  Copyright © 2017 Art4muslim. All rights reserved.
//

import Foundation

public class Url {

static let BaseURL : String = "https://www.lasihtuk.com"
    
    static let signUpUrl : String = BaseURL + "/foryourhealth/register_beneficiaries"
    static let loginUrl : String =  BaseURL + "/foryourhealth/login_beneficiaries"
    static let login_providerUrl : String =  BaseURL + "/foryourhealth/login_serviceprovider"

    static let citiesAllUrl : String =  BaseURL + "/foryourhealth/getallcityapps"
    static let citiesUrl : String =  BaseURL + "/foryourhealth/getallcityofcountrycityapps"

    static let countryUrl : String =  BaseURL + "/foryourhealth/getallcountrycityapps"
    static let sectionsUrl : String = BaseURL +  "/foryourhealth/getSections"

    static let requestcouponUrl : String = BaseURL +  "/foryourhealth/requestcoupon"

    static let forgetPasswordUrl : String =  BaseURL +  "/foryourhealth/forgetpassword_beneficiaries"
    static let providersUrl : String = BaseURL +  "/foryourhealth/getsearchserviceprovider"
    static let providersLocUrl : String = BaseURL +  "/foryourhealth/getsearchserviceproviderbymaps"

    static let adsImagesURL : String =   BaseURL + "/foryourhealth/getfristadver"

    static let getaboutappUrl : String = BaseURL +  "/foryourhealth/getaboutapp"
    static let gettermsconditionsappUrl : String = BaseURL +  "/foryourhealth/gettermsconditions"
    static let getallcouponuserappUrl : String = BaseURL +  "/foryourhealth/getallcouponuser"
    static let searchcouponUrl : String = BaseURL +  "/foryourhealth/searchcoupon"
    static let getcontactinfoUrl : String = BaseURL +  "/foryourhealth/getcontactinfo"

    static let enquiryUrl : String = BaseURL +  "/foryourhealth/insert_enquiry"
    static let suggestionUrl : String = BaseURL +  "/foryourhealth/insert_suggestion"
    static let complaintUrl : String = BaseURL +  "/foryourhealth/insert_complaint"
    static let getallsponsoredlinkUrl : String = BaseURL +  "/foryourhealth/getallsponsoredlink"

    static let profileUrl : String = BaseURL +  "/foryourhealth/update_beneficiaries"

    static let getserviceproviderimages : String =   BaseURL + "/foryourhealth/getserviceproviderimages"

}
