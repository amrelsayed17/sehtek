//
//  Dialog.swift
//  Nobil
//
//  Created by Art4muslim on 7/7/17.
//  Copyright © 2017 Art4muslim. All rights reserved.
//
import UIKit
import SwiftOverlays
import NVActivityIndicatorView



class Dialog: NSObject {

    
    
   static func showCircleProgressDialog() {

    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
    
    }
    
   static func hideCircleProgressDialog() {
        
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    static func logoutDialog(lang:Int,sender:UIViewController)
    {
        let alertController = UIAlertController(title: Strings.Data.logout_message[lang], message: "", preferredStyle: UIAlertControllerStyle.alert)
        let DestructiveAction = UIAlertAction(title: Strings.Data.cancel[lang], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
        }
        
        let okAction = UIAlertAction(title: Strings.Data.ok[lang], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in

          LogHelper.logout()

        }
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        sender.present(alertController, animated: true, completion: nil)
    }
    
    static func needToLoginDialog(sender:UIViewController)
    {
        let alertController = UIAlertController(title: "", message: "يجب عليك تسجيل الدخول اولا", preferredStyle: UIAlertControllerStyle.alert)
        let DestructiveAction = UIAlertAction(title: Strings.Data.cancel[Defaults.getLanguageIndex()], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
        }
        
        let okAction = UIAlertAction(title: Strings.Data.ok[Defaults.getLanguageIndex()], style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
            LogHelper.logout()
            
        }
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        sender.present(alertController, animated: true, completion: nil)
    }
    
}
