//
//  Defaults.swift
//  Nobil
//
//  Created by Art4muslim on 7/7/17.
//  Copyright © 2017 Art4muslim. All rights reserved.
//

import UIKit


class Defaults: NSObject {
    
    static var isLogin:Bool=true
     static var def:Bool = true

    static func checkResponse(response : GeneralResponseModel)  -> Bool {
        print(response)
        if(response.status == true){
            return true
        } else {
            return false
        }
    }
    
    static func getLanguage()  -> String {
        if(UserDefaults.standard.object(forKey: "lang") != nil){
            return UserDefaults.standard.object(forKey: "lang") as! String
        } else {
            return "ar"
        }
    }

    static func getLanguageIndex()  -> Int {
      
        if(getLanguage() == "en")
        {
            return 0
        }
        else
        {
        return 1
        }
    }
    
    static func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        defaults.synchronize()
    }
    

    
}

extension NSAttributedString {
    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            // not sure which is more reliable: String.Encoding.utf16 or String.Encoding.unicode
            return nil
        }
        guard let attributedString = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else {
            return nil
        }
        self.init(attributedString: attributedString)
    }
}
