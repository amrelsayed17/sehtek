//
//  ValidationHelper.swift
//  Nobil Worker
//
//  Created by Amr Elsayed on 9/28/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit

class ValidationHelper: NSObject {

   static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
