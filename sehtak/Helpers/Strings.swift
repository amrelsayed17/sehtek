

import UIKit

class Strings: NSObject {
    struct Data {
        
        static let cancel = ["Cancel","الغاء"]
        static let logout_message = ["Are you sure you want to logout from App?","هل حقا تريد تسجيل الخروج من التطبيق ؟"]
        static let close = ["Close","إغلاق"]
        static let ok = ["OK","موافق"]
        static let Location = ["Location","الموقع"]

        static let home = ["Home","الرئيسية"]
        static let profile = ["Profile","حسابى"]
        static let MyCart = ["My Coupons","كوبوناتى السابقة"]
        static let search_coupone = ["Search for coupon","البحث عن كوبون"]

        static let About = ["About App.","عن التطبيق"]
        static let Conditions = ["Conditions","الشروط والأحكام"]
        static let Share = ["Share","شارك التطبيق"]
        static let Contact = ["Contact Us","راسلنا"]
        static let Logout = ["Logout","تسجيل الخروج"]
        static let Current = ["Current","حالية"]
        static let Normal = ["Normal","العاديه"]
        static let Urgent = ["Urgent","العاجلة"]
        static let Completed = ["Completed","سابقة"]
        static let Gallary = ["Gallary","معرض الصور"]
        static let Camera = ["Camera","الكاميرا"]
        static let Phone = ["Phone","الجوال"]
        static let Password = ["Password","رمز المرور"]
        static let Forgrt = ["Forgrt Password !","نسيت رمز المرور !"]
        static let Login = ["Login","تسجيل الدخول"]
        static let Signup = ["Sign Up","تسجيل حساب جديد"]
        static let Skip = ["Skip","تخطى"]

        static let Name = ["Name","اسم المستخدم"]
        static let Email = ["Email","البريد الالكترونى"]
        static let Confirm_Password = ["Confirm Password","تأكيد رمز المرور"]
        static let Confirm = ["Confirm","تأكيد"]
        static let Offer = ["Offers","العروض"]
        static let All = ["All","الكل"]
        static let All_depts = ["All Departments","كل الاقسام"]

        static let Welcome = ["Welcome ...","مرحبا بك ..."]
        
        static let Message_Title = ["Message Title","عنوان الرسالة"]
        static let Message_Details = ["Message Details","تفاصيل الرسالة"]
        static let Send = ["Send","إرسال"]
        static let Next = ["Next","التالى"]

        static let Request_Now = ["Request Now !","اطلب الان !"]
        
        static let Date = ["Date :","التاريخ :"]
        static let Time = ["Time :","الوقت :"]
        static let elbladya_pic = ["painting Picture","صورة لوحة البلديه"]
        static let chassis_pic = ["Chassis Picture","صورة رقم الشاسية"]

        static let activate_message = ["Please , Enter the Acitivation Code","من فضلك ادخل كود التفعيل المرسل لكم"]
        static let Activate_My_Acoount = ["Activate My Acoount","تفعيل الحساب"]
        
        static let email_message = ["Please , Enter Your Email","من فضلك ادخل كود الايميل الخاص بكم"]
        static let phone_message = ["Please , Enter Your Phone","من فضلك ادخل رقم الهاتف الخاص بكم"]

        static let Back = ["Back","رجوع"]
        static let Details = ["Details","التفاصيل"]

        static let Order_Number = ["Order Number : ","رقم الطلب : "]
        static let Order_Date = ["Order Date : ","تاريخ الطلب : "]

        static let Cancel = ["Cancel","الغاء"]
        static let Complete = ["Complete","تم التنفيذ"]

        static let Comments = ["Comments","ملاحظات"]
        static let Rate_Message = ["Please, rate our service","من فضلك , قيم الخدمة"]
        
        static let Service_Provider = ["Service Provider","مزود الخدمة"]
        static let Normal_Order = ["Normal Order","طلب عادى"]
        static let Urgent_Order = ["Urgent Order","طلب عاجل"]

        static let Connection_Error = ["Please , Check your internet connection","من فضلك تأكد من اتصالك بالانترنت"]

        static let invalid_phone = ["Invalid Phone Number","رقم الهاتف غير صحيح"]

        static let invalid_password = ["Invalid Password","رمز المرور غير صحيح"]

        static let Invalid_Email = ["Invalid Email","الايميل غير صحيح"]
        static let Invalid_Name = ["Invalid Name","الاسم غير صحيح"]
        static let Password_Not_Confirmed = ["Password Not Confirmed","رمز المرور غير متطابق"]
        static let Invalid_Location = ["We Can't Determine Your Location","غير قادر على تحديد موقعك"]

        static let Select_Image = ["Please, Select Image","يجب تحديد الصورة"]
        static let Select_Date = ["Please, Select Date And Time","يجب تحديد الوقت والتاريخ"]

        static let No_Orders = ["There Is No Orders In This Department","لا توجد طلبات فى هذا القسم"]

        static let Invalid_Activation_Code = ["Invalid Activation Code","كود التفعيل غير صحيح"]

        static let Sign_Up = ["Sign Up","التسجيل"]
        static let Forget_Password = ["Forget Password","استرجاع رمز المرور"]
        static let Activation = ["Activation","التفعيل"]
        static let Nobel = ["Nobel","فاكهتى"]

        static let Home = ["Home","الرئيسية"]
        static let Products = ["Products","المنتجات"]

        static let Order_Data = ["Order's Data","بيانات الطلب"]
        static let Select_Services_Types = ["Select Services Types","تحديد نوع الخدمات"]

        static let map = ["Map","الخريطة"]
        static let companies = ["List","القائمة"]

        static let show = ["Show Coupon","عرض الكوبون"]
        static let print = ["Print Coupon","طباعة الكوبون"]
        static let send = ["Send Coupon","ارسال الكوبون"]
        static let client = ["Client","مستخدم"]
        static let search = ["Search","بحث"]
        static let card_provider = ["Card provider","مقدم البطاقة"]
        static let Card_Owner = ["Card Owner","حامل البطاقة"]
        static let Status = ["Status","الحالة"]
        static let welcome = ["Hello ...","مرحبا ..."]

        static let Enquiry = ["Enquiry","استفسار"]
        static let Suggestion = ["Suggestion","اقتراح"]
        static let Complaint = ["Complaint","شكوى"]
        static let Department = ["Department","القسم"]
        static let City = ["City","المدينة"]
        static let Country = ["Country","الدولة"]
        static let friend = ["Send to friend","ارسال الى صديق"]
        static let discount = [" discount "," خصم "]

        static let Show_details = ["Show details","عرض التفاصيل"]
        static let Show_location = ["Show direction","عرض المسار للموقع"]

    }
}
