//
//  LogHelper.swift
//  Nobil
//
//  Created by Amr Elsayed on 8/21/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit

class LogHelper: NSObject {

    static func startMainHome()
    {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let main_page_navigation = mainStoryBoard.instantiateViewController(withIdentifier: "main_nav") as! SWRevealViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = main_page_navigation
    }
    
    static func logout()
    {
        let lang:String = (UserDefaults.standard.object(forKey: "lang") as? String)!
        Defaults.resetDefaults()
        UserDefaults.standard.set(lang, forKey: "lang")

        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let main_page_navigation = mainStoryBoard.instantiateViewController(withIdentifier: "start_nav") as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = main_page_navigation
    }
    

}
