//
//  SideMenu.swift
//  Nobil
//
//  Created by Art4muslim 
//  Copyright © 2017 Art4muslim. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import XLActionController


class SideMenu: UIViewController , UITableViewDelegate , UITableViewDataSource , UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var icon_image:UIImage?

    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var user_image: UIImageView!
    var lang:Int=0
    @IBOutlet weak var tableView: UITableView!
    var user:UserModel?
    
    @IBOutlet weak var welcome_message: UILabel!
    var titles:NSArray=NSArray()
    var storyboard_ids:NSArray=NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        user_image.clipsToBounds=true
        user_image.layer.cornerRadius=15
        
        ViewsHelper.viewDirections(view, lang: Defaults.getLanguage())
        
    }
 
    
    @objc func viewProfile() {
        
        if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser != nil
        {
            let  storyboard = UIStoryboard(name: "Main", bundle: nil)
            let destinationViewController = storyboard.instantiateViewController(withIdentifier: "profile")
            self.revealViewController().frontViewController=destinationViewController
            self.revealViewController().rightRevealToggle(animated: true)
        }
        else
        {
            SweetAlert().showAlert("يجب تسجيل الدخول اولا", subTitle: "", style: AlertStyle.warning)

        }

    }
    
    func showUserImage()  {
        
//        if (user?.image != nil)
//        {
//            Alamofire.request(Url.userImagesURL + (user?.image!)!).responseImage { response in
//                if let image = response.result.value {
//                    self.user_image.image=image
//                    self.user_image.clipsToBounds=true
//                    self.user_image.layer.cornerRadius=5
//                }
//            }
//        }
    }
    
    func showUserName()  {
        
        if let name=user?.nameuser
        {
            self.user_name.text=name
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        lang=Defaults.getLanguageIndex()
        self.welcome_message.text=Strings.Data.welcome[lang]

        if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser != nil
        {
            user=(UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)!
            showUserName()
            showUserImage()
            
            if((UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.type == "1")
            {
                storyboard_ids=["profile","my","main_nav","share","about","conditions","contact"]
                titles=[Strings.Data.profile[lang],Strings.Data.MyCart[lang],Strings.Data.Offer[lang],Strings.Data.Share[lang],Strings.Data.About[lang],Strings.Data.Conditions[lang],Strings.Data.Contact[lang],Strings.Data.Logout[lang]]

            }
            else
            {
                storyboard_ids=["profile","show","main_nav","share","about","conditions","contact"]
                titles=[Strings.Data.profile[lang],Strings.Data.search_coupone[lang],Strings.Data.Offer[lang],Strings.Data.Share[lang],Strings.Data.About[lang],Strings.Data.Conditions[lang],Strings.Data.Contact[lang],Strings.Data.Logout[lang]]

            }
        }
        else
        {
            storyboard_ids=["profile","show","main_nav","share","about","conditions","contact"]
            titles=[Strings.Data.profile[lang],Strings.Data.MyCart[lang],Strings.Data.Offer[lang],Strings.Data.Share[lang],Strings.Data.About[lang],Strings.Data.Conditions[lang],Strings.Data.Contact[lang],Strings.Data.Login[lang]]

        }

        
        

        self.tableView.reloadData()
    }
    
    
    //MARK: - Table View Data Source
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuCell
        cell.title.text=(titles[indexPath.row] as! String)
        ViewsHelper.viewDirections(cell, lang: Defaults.getLanguage())

        return cell;
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row==7 {
            
            if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser != nil
            {
                Dialog.logoutDialog(lang: Defaults.getLanguageIndex(), sender: self)

            }
            else
            {
                LogHelper.logout()
            }
        }
        else if(indexPath.row == 3)
        {
            // share
            let message = "تطبيق لصحتك"
            if let link = NSURL(string: Url.BaseURL)
            {
                let objectsToShare = [message,link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        else
        {
            
            if (UserDefultsHelper.getObjectDefault(key: UserDefultsHelper.USER_MODEL) as? UserModel)?.iduser == nil
            {
                if(indexPath.row == 0 || indexPath.row == 1 )
                {
                    Dialog.needToLoginDialog(sender: self)
                }
                else
                {
                    let  storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: storyboard_ids[indexPath.row] as! String)
                    self.revealViewController().frontViewController=destinationViewController
                }

            }
            else
            {
                let  storyboard = UIStoryboard(name: "Main", bundle: nil)
                let destinationViewController = storyboard.instantiateViewController(withIdentifier: storyboard_ids[indexPath.row] as! String)
                self.revealViewController().frontViewController=destinationViewController
            }

        }
        
        if(Defaults.getLanguage()=="ar")
        {
            self.revealViewController().rightRevealToggle(animated: true)
        }
        else
        {
            self.revealViewController().revealToggle(animated: true)
            
        }
        
    }
    

}

