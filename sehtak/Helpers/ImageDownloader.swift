//
//  ImageDownloader.swift
//  Helper
//


import UIKit
import SDWebImage

class ImageDownloader: NSObject {
    
    static func downloadImage(imageView:UIImageView , url:String , placeHolder:String , showIndicator:Bool)  {
        imageView.sd_addActivityIndicator()
        imageView.sd_setIndicatorStyle(.gray)
        imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: placeHolder)  )
    }

}
