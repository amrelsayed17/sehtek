//
//  ProviderListModel.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/1/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import ObjectMapper

class ProviderListModel:  GeneralResponseModel {
    
    var data: [City]?
    
    override func mapping(map: Map) {
        data <- map ["data"]
        message <- map["message"]
        status <- map["status"]
    }
}

class Provider: Mappable {
    var id: String?
    var name: String?
    var image: String?
    var details: String?
    var discount: String?
    var longitude: String?
    var latitude: String?
    var mobile: String?
    var email: String?

    required init?(map: Map){
        
    }
    
     init(){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        image <- map["image"]
        discount <- map["discount"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        mobile <- map["mobile"]
        email <- map["email"]

        if(UserDefaults.standard.object(forKey: "lang") as? String == "en")
        {
            name <- map["nameen"]
            details <- map["detailsen"]

        }
        else
        {
            name <- map["name"]
            details <- map["details"]

        }
        
    }
}

