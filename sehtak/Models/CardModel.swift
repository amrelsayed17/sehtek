//
//  CardModel.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/3/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import ObjectMapper

class CardModel: Mappable {
    var service_provider: String?
    var user_name: String?
    var code: String?
    var active: String?

    required init?(map: Map){
        
    }
    
    init(){
        
    }
    
    func mapping(map: Map) {
        user_name <- map["user_name"]
        code <- map["code"]
        active <- map["active"]

        
        if(UserDefaults.standard.object(forKey: "lang") as? String == "en")
        {
            service_provider <- map["service_provideren"]
        }
        else
        {
            service_provider <- map["service_provider"]
        }
        
    }
}

