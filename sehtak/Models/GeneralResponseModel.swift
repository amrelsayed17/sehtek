//
//  GeneralResponseModel.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/5/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import ObjectMapper

class GeneralResponseModel: Mappable {

    var message: String?
    var status: Bool?
    var code: String?

    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        status <- map["status"]
        code <- map["code"]

    }
    
    init(message:String , status:Bool) {
        self.message=message
        self.status=status
    }
}

