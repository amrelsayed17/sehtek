//
//  SponserModel.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/4/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import ObjectMapper

class SponserModel: Mappable {
    var name: String?
    var link: String?
    
    required init?(map: Map){
        
    }
    
    init(){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        link <- map["link"]
        
    }
}

