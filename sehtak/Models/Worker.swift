//
//  Worker.swift
//  Nobil
//
//  Created by Amr Elsayed
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class Worker: NSObject, MKAnnotation {
    let title: String?
    let _id: String
    let phone: String
    let coordinate: CLLocationCoordinate2D
    let email : String
    
    init(title: String, _id: String, phone: String, coordinate: CLLocationCoordinate2D,email:String) {
        self.title = title
        self._id = _id
        self.phone = phone
        self.coordinate = coordinate
        self.email = email
        super.init()
    }
    
    var subtitle: String? {
        //return Strings.Data.Service_Provider[Defaults.getLanguageIndex()]
        return self.title
    }
}
