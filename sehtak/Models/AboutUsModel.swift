//
//  AboutUsModel.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/13/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import ObjectMapper

class AboutUsModel : GeneralResponseModel {
    
    var text: String?
    
    override func mapping(map: Map) {
        text <- map["text"]
        message <- map["message"]
        status <- map["status"]
    }
}

class AboutUs: Mappable {
    var id: String?
    var name: String?
    var text: String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        text <- map["text"]
        
    }
}
