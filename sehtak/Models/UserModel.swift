//
//  UserModel.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/7/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import ObjectMapper
import Foundation

class UserModel: NSObject, Mappable , NSCoding{
    
    var message: String?
    var status: Bool?
    var iduser: String?
    var nameuser: String?
    var email: String?
    var mobile: String?
    var password: String?
    var type: String?   // 1-> cient  , 2-> provider
 
      func mapping(map: Map) {
        message <- map["message"]
        status <- map["status"]
        iduser <- map["id_user"]
        nameuser <- map["name_user"]
        mobile <- map["mobile_user"]

           }
    
    required init?(map: Map){
    }
    
    override init(){
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        let iduser = aDecoder.decodeObject(forKey: "id_user") as? String
        let nameuser = aDecoder.decodeObject(forKey: "name_user") as? String
        let mobile = aDecoder.decodeObject(forKey: "mobile_user") as? String
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let password = aDecoder.decodeObject(forKey: "password") as? String
        let type = aDecoder.decodeObject(forKey: "type") as? String


        self.init(iduser:iduser!,nameuser:nameuser!,email:email!,password:password! , mobile:mobile!  , type:type!)
    }
    
    
    
    init( iduser:String,nameuser:String,email:String,password:String,mobile:String,type:String ) {
        self.iduser=iduser
        self.nameuser=nameuser
        self.email=email
        self.password=password
        self.mobile=mobile
        self.type=type
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(iduser, forKey: "id_user")
        aCoder.encode(nameuser,    forKey: "name_user")
        aCoder.encode(email,    forKey: "email")
        aCoder.encode(password,    forKey: "password")
        aCoder.encode(mobile,    forKey: "mobile_user")
        aCoder.encode(type,    forKey: "type")

    }
}
