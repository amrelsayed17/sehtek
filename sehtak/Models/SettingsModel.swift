//
//  SettingsModel.swift
//  sehtak
//
//  Created by Amr Elsayed on 9/3/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit
import ObjectMapper

class SettingsModel: Mappable {
    var siteemail: String?
    var facebook: String?
    var twitter: String?
    var youtube: String?
    var instagram: String?
    var mob: String?


    required init?(map: Map){
        
    }
    
    init(){
        
    }
    
    func mapping(map: Map) {
        siteemail <- map["siteemail"]
        facebook <- map["facebook"]
        twitter <- map["twitter"]
        youtube <- map["youtube"]
        instagram <- map["instagram"]

        mob <- map["mob"]

    }
}

