//
//  CitiesListModel.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/5/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import ObjectMapper

class CitiesListModel:  GeneralResponseModel {

    var data: [City]?
    
    override func mapping(map: Map) {
        data <- map ["data"]
        message <- map["message"]
        status <- map["status"]
    }
}

class City: Mappable {
    var id: String?
    var name: String?
    
    required init?(map: Map){
        
    }
    init(id:String,name:String) {
        self.id=id
        self.name=name
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        if(UserDefaults.standard.object(forKey: "lang") as? String == "en")
        {
            name <- map["nameen"]
        }
        else
        {
            name <- map["name"]
        }
    }
}
