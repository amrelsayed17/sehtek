//
//  ProductsListModel.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/14/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductsListModel:  GeneralResponseModel {
    
    var data: [Product]?
    
    override func mapping(map: Map) {
        data <- map["data"]
        message <- map["message"]
        status <- map["status"]
    }
}

class Product: Mappable {
    var id: String?
    var product_name: String?
    var product_netPrice: String?
    var image: String?
    var product_long_desc:String?
    var product_quantity:String?

    required init?(map: Map){
        
    }
    
    init() {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        product_name <- map["product_name"]
        product_netPrice <- map["product_netPrice"]
        image <- map["image"]
        product_long_desc <- map["product_long_desc"]
        product_quantity <- map["product_quantity"]

    }
}
