//
//  AlertCell.swift
//  Fruta
//
//  Created by Amr Elsayed on 1/15/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit

class AlertCell: UITableViewCell {

    @IBOutlet weak var alert_date: UILabel!
    @IBOutlet weak var alert_title: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initCell(coupone:CouponModel) {
        self.alert_title.text=coupone.service_provider
        self.alert_date.text=coupone.code
    }
    
    
}
