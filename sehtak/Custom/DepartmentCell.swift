//
//  DepartmentCell.swift
//  FakhtySwift
//
//  Created by Amr Elsayed on 11/13/17.
//  Copyright © 2017 art4muslim. All rights reserved.
//

import UIKit

class DepartmentCell: UICollectionViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   func initCell(department:Department) {
    title.text=department.name
   // icon.contentMode = .scaleAspectFill
 //   ImageDownloader.downloadImage(imageView: icon, url: department.image!, placeHolder: "back", showIndicator: true)
    
//    if(department.image != nil)
//    {
//        ImageDownloader.downloadImage(imageView: icon, url: Url.categoryImagesURL + department.image!, placeHolder: "back", showIndicator: true)
//    }
 }
}
