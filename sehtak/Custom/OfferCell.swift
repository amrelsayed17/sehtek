//
//  OfferCell.swift
//  discount
//
//  Created by Amr Elsayed on 7/1/18.
//  Copyright © 2018 Amr Elsayed. All rights reserved.
//

import UIKit

class OfferCell: UICollectionViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initCell(offer:Provider ) {
        
        name.text=offer.name ?? ""
        if(offer.image != nil)
        {
            ImageDownloader.downloadImage(imageView: icon, url:offer.image!, placeHolder: "back", showIndicator: true)
        }
    }
}

